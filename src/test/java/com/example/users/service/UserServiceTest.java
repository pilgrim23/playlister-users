package com.example.users.service;

import com.example.users.model.User;
import com.example.users.model.UserList;
import com.example.users.model.UserNotFoundException;
import com.example.users.table.UserTable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Mock
    UserTable userTable;
    private UserService userService;
    private List<User> testUsers;

    @BeforeEach
    void setup() {
        userService = new UserService(userTable);
        testUsers = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            User u = new User("user" + i, "pwd" + i, i + "@mail.net");
            testUsers.add(u);
        }
    }

    @Test
    void getUsers_NoParams_ReturnsUserList() {
        when(userTable.findAll()).thenReturn(testUsers);
        UserList users = userService.getUsers();
        assertNotNull(users);
        assertEquals(testUsers.size(), users.getUsers().size());
    }

    @Test
    void getUsers_NoParams_ReturnsNull() {
        when(userTable.findAll()).thenReturn(testUsers);
        UserList users = userService.getUsers();
        assertNotNull(users);
        assertEquals(testUsers.size(), users.getUsers().size());
    }

    @Test
    void addUser_User_ReturnsCreatedUser() {
        User user = testUsers.get(0);
        when(userTable.save(any(User.class))).thenReturn(user);
        User actual = userService.addUser(user);

        assertNotNull(actual);
        assertEquals(user.getUsername(), actual.getUsername());
    }

    @Test
    void getUser_Username_ReturnsUser() {
        User user = testUsers.get(0);
        when(userTable.findByUsername(anyString())).thenReturn(user);
        User actual = userService.getUser(user.getUsername());
        assertNotNull(actual);
        assertEquals(user.getUsername(), actual.getUsername());
    }

    @Test
    void getUser_Username_ReturnsNull() {
        when(userTable.findByUsername(anyString())).thenReturn(null);
        User actual = userService.getUser("noSuchUsername");
        assertNull(actual);
    }

    @Test
    void deleteUser_Username_Success() {
        User user = testUsers.get(0);
        when(userTable.findByUsername(anyString())).thenReturn(user);
        userService.deleteUser(user.getUsername());
        verify(userTable).delete(any(User.class));
    }

    @Test
    void deleteUser_Username_Exception() {
        when(userTable.findByUsername(anyString())).thenReturn(null);
        assertThrows(UserNotFoundException.class,
                () -> userService.deleteUser("noSuchUsername"));
    }
}
