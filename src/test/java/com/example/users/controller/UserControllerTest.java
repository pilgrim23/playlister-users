package com.example.users.controller;

import com.example.users.model.User;
import com.example.users.model.UserList;
import com.example.users.model.UserNotFoundException;
import com.example.users.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;


import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
public class UserControllerTest {

    private final String URI = "/api/users";
    @Autowired
    MockMvc mockMvc;
    @MockBean
    UserService userService;

    List<User> testUsers;

    @BeforeEach
    void setup() {
        testUsers = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            User u = new User("user" + i, "pwd" + i, i + "@mail.net");
            testUsers.add(u);
        }
    }

    // GET /api/users returns all autos 200
    @Test
    void getUsers_NoParams_ReturnsAllUsers() throws Exception {
        when(userService.getUsers()).thenReturn(new UserList(testUsers));

        mockMvc.perform(get(URI))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.users", hasSize(5)));
    }

    // GET /api/users returns 204 for no results
    @Test
    void getUsers_NoParams_NoContent() throws Exception {
        when(userService.getUsers()).thenReturn(null);

        mockMvc.perform(get(URI))
                .andExpect(status().isNoContent());
    }

    // POST /api/users returns created user 200
    @Test
    void addUser_User_ReturnsCreatedUsers() throws Exception {
        User user = testUsers.get(0);
        when(userService.addUser(any(User.class))).thenReturn(user);

        mockMvc.perform(post(URI, User.class)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJSON(user)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("username").value("user0"));
    }

    // POST /api/users return bad request 400
    @Test
    void addUser_User_BadRequest() throws Exception {
        User user = new User("", "pwd", "i@g.com");
        when(userService.addUser(any(User.class))).thenReturn(user);

        mockMvc.perform(post(URI, User.class)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJSON(user)))
                .andExpect(status().isBadRequest());

    }

    @Test
    void addUser_User_BadRequest2() throws Exception {
        User user = new User("usr", null, "i@e.com");
        when(userService.addUser(any(User.class))).thenReturn(user);

        mockMvc.perform(post(URI, User.class)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJSON(user)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void addUser_User_BadRequest3() throws Exception {
        User user = new User("usr", "pwd", "");
        when(userService.addUser(any(User.class))).thenReturn(user);

        mockMvc.perform(post(URI, User.class)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJSON(user)))
                .andExpect(status().isBadRequest());
    }


    // GET /api/users/username returns user with username 200

    @Test
    void getUser_Username_ReturnsUser() throws Exception {
        User user = new User("usr", "pwd", "i@g.com");
        when(userService.getUser(anyString())).thenReturn(user);

        mockMvc.perform(get(URI + "/" + user.getUsername()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("username").value("usr"));
    }

    // GET /api/users/username returns no content if not found 204
    @Test
    void getUser_Username_NoContent() throws Exception {
        when(userService.getUser(anyString())).thenReturn(null);

        mockMvc.perform(get(URI + "/noUserFound"))
                .andExpect(status().isNoContent());
    }

    // DELETE /api/users/username accepted deletes user 200
    @Test
    void deleteUser_Username_Accepted() throws Exception {
        User user = new User("usr", "pwd", "i@g.com");

        mockMvc.perform(delete(URI + "/" + user.getUsername()))
                .andExpect(status().isAccepted());
        verify(userService).deleteUser(anyString());
    }

    // DELETE /api/users/username no content if not found 204
    @Test
    void deleteUser_Username_NoContent() throws Exception {
        User user = new User("usr", "pwd", "i@g.com");

        doThrow(new UserNotFoundException()).when(userService).deleteUser(anyString());

        mockMvc.perform(delete(URI + "/" + user.getUsername()))
                .andExpect(status().isNoContent());
    }

    public String toJSON(Object auto) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(auto);
    }

}
