package com.example.users;

import com.example.users.model.User;
import com.example.users.model.UserList;
import com.example.users.table.UserTable;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.TestPropertySource;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@TestPropertySource(locations= "classpath:application-test.properties")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class UsersApplicationTests {

    private final String URI = "/api/users";

    @Autowired
    TestRestTemplate restTemplate;

    @Autowired
    UserTable userTable;

    List<User> testUsers;

    @BeforeEach
    void setup() {
        testUsers = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            User u = new User("user" + i, "pwd" + i, i + "@mail.net");
            testUsers.add(u);
        }
        userTable.saveAll(testUsers);
    }

    @AfterEach
    void tearDown() {
        userTable.deleteAll();
    }

    @Test
    void contextLoads() {
    }

    @Test
    void getUsers_NoParams_ReturnsUserList() {
        ResponseEntity<UserList> response = restTemplate.getForEntity(URI, UserList.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(testUsers.size(), response.getBody().getUsers().size());
    }

    @Test
    void getUsers_NoParams_ReturnsNoContent() {
        userTable.deleteAll();
        ResponseEntity<UserList> response = restTemplate.getForEntity(URI, UserList.class);

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    void addUser_User_ReturnsCreatedUser() throws JsonProcessingException {
        User user = new User("name", "pass", "i@g.com");
        String body = toJSON(user);
        ResponseEntity<User> response = restTemplate.postForEntity(URI, getHeaders(body), User.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(testUsers.size() + 1, response.getBody().getId());
        assertEquals(user.getUsername(), response.getBody().getUsername());
    }

    @Test
    void addUser_User_BadRequest() throws JsonProcessingException {
        User user = new User("", "pass", "i@g.com");
        String body = toJSON(user);
        ResponseEntity<User> response = restTemplate.postForEntity(URI, getHeaders(body), User.class);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    void addUser_User_BadRequest2() throws JsonProcessingException {
        User user = new User("user", null, "i@g.com");
        String body = toJSON(user);
        ResponseEntity<User> response = restTemplate.postForEntity(URI, getHeaders(body), User.class);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    void addUser_User_BadRequest3() throws JsonProcessingException {
        User user = new User("user", "pwd", "");
        String body = toJSON(user);
        ResponseEntity<User> response = restTemplate.postForEntity(URI, getHeaders(body), User.class);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    void getUser_Username_ReturnsUser() {
        String username = "user0";
        ResponseEntity<User> response = restTemplate.getForEntity(URI + "/" + username, User.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    void getUser_Username_NoContent() {
        String username = "userNotFound";
        ResponseEntity<User> response = restTemplate.getForEntity(URI + "/" + username, User.class);

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    void deleteUser_Username_Accepted() {
        String username = "user0";
        ResponseEntity<User> response = restTemplate.getForEntity(URI + "/" + username, User.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());

        restTemplate.delete(URI + "/" + username);

        ResponseEntity<User> emptyResponse = restTemplate.getForEntity(URI + "/" + username, User.class);

        assertEquals(HttpStatus.NO_CONTENT, emptyResponse.getStatusCode());
    }

    @Test
    void deleteUser_Username_NoContent() {
        ResponseEntity<UserList> firstResponse = restTemplate.getForEntity(URI, UserList.class);

        assertEquals(HttpStatus.OK, firstResponse.getStatusCode());
        assertNotNull(firstResponse.getBody());

        restTemplate.delete(URI + "/userNotFound");

        ResponseEntity<UserList> lastResponse = restTemplate.getForEntity(URI, UserList.class);

        assertEquals(HttpStatus.OK, lastResponse.getStatusCode());
        assertNotNull(lastResponse.getBody());
        assertEquals(firstResponse.getBody().getUsers().size(), lastResponse.getBody().getUsers().size());
    }

    private String toJSON(Object o) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(o);
    }

    public HttpEntity<?> getHeaders(String body) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        return new HttpEntity<>(body, headers);
    }
}
