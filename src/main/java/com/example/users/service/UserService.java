package com.example.users.service;

import com.example.users.model.User;
import com.example.users.model.UserList;
import com.example.users.model.UserNotFoundException;
import com.example.users.table.UserTable;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private final UserTable userTable;

    public UserService(UserTable userTable) {
        this.userTable = userTable;
    }

    public UserList getUsers() {
        UserList foundUsers = new UserList(userTable.findAll());
        if (foundUsers.isEmpty()) return null;
        return foundUsers;
    }

    public User addUser(User user) {
        return userTable.save(user);
    }

    public User getUser(String username) {
        return userTable.findByUsername(username);
    }

    public void deleteUser(String username) {
        User foundUser = userTable.findByUsername(username);
        if (foundUser == null) throw new UserNotFoundException();
        userTable.delete(foundUser);
    }
}
