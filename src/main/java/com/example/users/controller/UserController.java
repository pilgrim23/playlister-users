package com.example.users.controller;

import com.example.users.model.User;
import com.example.users.model.UserList;
import com.example.users.model.UserNotFoundException;
import com.example.users.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<UserList> getUsers() {
        UserList users = userService.getUsers();
        if (users == null) return ResponseEntity.noContent().build();
        return ResponseEntity.ok(users);
    }

    @PostMapping
    public ResponseEntity<User> addUser(@RequestBody User user) {
        if (inValidUser(user)) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(userService.addUser(user));
    }

    @GetMapping("/{username}")
    public ResponseEntity<User> getUser(@PathVariable String username) {
        User user = userService.getUser(username);
        if (user == null) return ResponseEntity.noContent().build();
        return ResponseEntity.ok(user);
    }

    @DeleteMapping("/{username}")
    public ResponseEntity<?> deleteUser(@PathVariable String username) {
        try {
            userService.deleteUser(username);
        } catch (UserNotFoundException e) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.accepted().build();
    }

    private boolean inValidUser(User user) {
        return (user.getUsername() == null || user.getEmail() == null || user.getPassword() == null
                || user.getUsername().equals("") || user.getPassword().equals("") || user.getEmail().equals(""));
    }

}
