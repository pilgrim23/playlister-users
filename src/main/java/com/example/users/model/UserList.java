package com.example.users.model;

import java.util.ArrayList;
import java.util.List;

public class UserList {
    public List<User> users;

    public UserList(List<User> users) {
        this.users = users;
    }

    public UserList() {
        users = new ArrayList<>();
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public boolean isEmpty() {
        return this.users.isEmpty();
    }
}
