package com.example.users.table;

import com.example.users.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserTable extends JpaRepository<User, Long> {

    @Query(nativeQuery = true, value = "Select * from users where username = ?")
    User findByUsername(String username);
}
